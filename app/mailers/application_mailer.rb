class ApplicationMailer < ActionMailer::Base
  default from: "WorldSoccerCommunite@example.com"
  layout 'mailer'
end